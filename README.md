# HTML Produktionsumgebung mit Bootstrap 3 (SASS)

## Initiales Setup
Dieser Prozess muss Projektunabhängig nur einmal auf dem PC durchgeführt werden.

### Node.js Version 4.x LTS installieren
https://nodejs.org/

Notwendige Installationsoptionen
* Node.js runtime
* npm package manager
* "Add to PATH"

### Gulp installieren
Konsole:
```
npm install --global gulp-cli
```


 
## Projekt Setup
Dieser Prozess muss initial für jedes Projekt einmal durchgeführt werden.

### Projekt herunterladen und konfigurieren
1.  Dieses Projektarchiv als Zip laden und im gewünschten Ordner entpacken.

### Projekt initialisieren

Konsole öffnen und in den Unterordner "**production/**" navigieren
Node Module installieren:
```
npm install
```

Default Gulp-Task ausführen:
```
gulp
```


## Watch Task
Der Watchtask kompiliert das CSS automatisch, weist auf HTML Fehler hin und öffnet einen Vorschauserver.

Watch Task ausführen:
Konsole öffnen und in den Unterordner "**production/**" navigieren
```
gulp watch
```