
// Hier Zielsystempfad angeben
var systemDist = false;
//var systemDist = "../typo3conf/ext/kw_website/";


var gulp = require('gulp'),
    browserSync = require('browser-sync').create(),
    fs = require('fs'),
	mustache = require("gulp-mustache"),
    plugins = require('gulp-load-plugins')();

var paths = {
    scripts: ['js/**/*.js'],
    styles: ['css/**/*.css'],
    sass: ['sass/**/*.scss'],
    html: ['*.html'],
    images: ['images/**/*'],
    fonts: ['fonts/*']
};

gulp.task('fontawesome.src', function () {
    return gulp.src('**/*', {cwd: './node_modules/font-awesome/fonts/'})
        .pipe(gulp.dest('./src/fonts/'));
});
gulp.task('bootstrapjs.src', function () {
    return gulp.src('./node_modules/bootstrap/dist/js/bootstrap.min.js')
        .pipe(gulp.dest('./src/js/libs/'));
});
gulp.task('jquery.src', function () {
    return gulp.src('./node_modules/jquery/dist/jquery.min.js')
        .pipe(gulp.dest('./src/js/libs/'));
});
gulp.task('variables.src', function (done) {
    if( !fs.existsSync('./src/sass/_variables.scss') ) {
        return gulp.src('./node_modules/bootstrap/scss/_variables.scss')
        .pipe(gulp.dest('./src/sass/'));
    }
    done();
});

gulp.task('fonts.dist', function () {
    return gulp.src('**/*', {cwd: './src/fonts'})
        .pipe(gulp.dest('./dist/fonts/'))
        .pipe(browserSync.stream());
});
gulp.task('images.dist', function () {
    return gulp.src('**/*', {cwd: './src/images/'})
        .pipe(gulp.dest('./dist/images/'))
        .pipe(browserSync.stream());
});
gulp.task('js.dist', function () {
    return gulp.src('**/*.js', {cwd: './src/js/'})
        .pipe(gulp.dest('./dist/js/'))
        .pipe(browserSync.stream());
});
gulp.task('css.dist',gulp.series('variables.src', function () {
    return gulp.src('style.scss', {cwd: './src/sass'})
        .pipe(plugins.sass().on('error', plugins.sass.logError))
        .pipe(plugins.autoprefixer())
        .pipe(gulp.dest('./dist/css/'))
        .pipe(browserSync.stream());
}));
gulp.task('html.dist', function () {
    return gulp.src('*.html', {cwd: './src/'})
        .pipe(mustache())
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.stream());
});

gulp.task('all.src' , gulp.series(
    'fontawesome.src',
    'bootstrapjs.src',
    'jquery.src',
    'variables.src'
));


gulp.task('all.dist',gulp.series(
    'fonts.dist',
    'images.dist',
    'js.dist',
    'css.dist',
    'html.dist'
));

gulp.task('all.live', function(done){
    if(systemDist){
        return gulp.src('**/*', {cwd: './dist/'})
            .pipe(gulp.dest(systemDist));
    }
    done();
});

gulp.task('init', gulp.series('all.src', function(done) {done();}));
gulp.task('dist', gulp.series('all.dist', function(done) {done();}));
gulp.task('live', gulp.series('all.live', function(done) {done();}));


gulp.task('default',function(){
    gulp.start('init');
    setTimeout(function() { gulp.start('dist'); }, 1500); 
    setTimeout(function() { gulp.start('live'); }, 4000);
});

//Watch
gulp.task('watch', gulp.series('all.dist', function (done) {
    browserSync.init({
        server: {
            baseDir: "dist"
        }
    });
    gulp.watch("src/sass/**/*.scss", gulp.series('css.dist'));
    gulp.watch(["src/*.html","src/partials/*.mustache"],gulp.series('html.dist'));
    gulp.watch("src/js/**/*.js",gulp.series('js.dist'));
    gulp.watch("src/fonts/**/*",gulp.series('fonts.dist'));
    gulp.watch("src/images/**/*",gulp.series('images.dist'));

    done();
}));
