// Accordion
$('.accordion .acc-card-header').on('click', function () {
    if ($(this).parent().hasClass('active')) {
        $(this).parent().removeClass('active');
    } else {
        $('.accordion .acc-card').removeClass('active');
        $(this).parent().addClass('active');
    }
})

// Video
$('.video-div').on('click', function () {
    if ($(this).hasClass('inactive')) {
        console.log($(this>'video'));
        $(this).removeClass('inactive');
        $('video', this).attr('controls', true);
        $('video', this).trigger('play');
    }
})

// Desktop Flact Contact Form
$('.flag-mail').on('click', function () {
    if(!$(this).hasClass('active')) {
        $(this).addClass('active');
        $('.contact-form-desktop').removeClass('d-none');
        console.log('activate');
    } else if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $('.contact-form-desktop').addClass('d-none');
        console.log('deactivate');
    }
})

$('.flag-phone').on('click', function () {
    if(!$(this).hasClass('active')) {
        $(this).addClass('active');
    } else if ($(this).hasClass('active')) {
        $(this).removeClass('active');
    }
})

$('.contact-exit').on('click', function () {
    if(!$('.contact-form-desktop').hasClass('d-none')) {
        $('.contact-form-desktop').addClass('d-none');
        $('.flag-mail').removeClass('active');
    }
})

$(window).on('resize', function () {
    if (!$('.contact-form-desktop').hasClass('d-none')) {
        $('.contact-form-desktop').addClass('d-none');
        $('.flag-mail').removeClass('active');
    }
})


// Mobile Contact Flag Box
$('.mobile-flag-mail').on('click', function () {
    if($('.mobile-flag.active').length == 0) {
        $('.mobile-flag').addClass('active');
    }
})

$('.mobile-flag-close').on('click', function () {
        $('.mobile-flag.active').removeClass('active');
})


// Contact form: Set iframe
$(document).ready(function() {
    $(window).on('CookiebotOnAccept CookiebotOnDecline CookiebotOnConsentReady', function () {
        $('#contact-form-iframe').append('<iframe src="https://www.richter-helm.eu/index.php?id=51" id="microbial-production-contact" frameborder="0" class=""></iframe>');
    })
});


// Matomo-Tracking von Klick auf Kontakt-Flags
$('.flag-phone').on('click', function() {
    console.log('phone flag clicked');
    _paq.push(['trackEvent', 'Flag Phone', 'Click']);
})

$('.flag-linkedin').on('click', function() {
    console.log('linkedin flag clicked');
    _paq.push(['trackEvent', 'Flag LinkedIn', 'Click']);
})

$('.flag-mail').on('click', function() {
    console.log('form flag clicked');
    _paq.push(['trackEvent', 'Flag Contact Form', 'Click']);
})

$('.mobile-flag-mail').on('click', function() {
    console.log('mobile contact flag clicked');
    _paq.push(['trackEvent', 'Mobile Flag Contact', 'Click']);
})